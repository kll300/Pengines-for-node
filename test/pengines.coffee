pengines = require './../src/pengines'

tests =

    setUp : (callback) ->
        callback()
    
    changeOnDollar :  (test) ->
        
        test.expect(292)
        
        #Change for a dollar, from tutorial: http://www.cpp.edu/~jrfisher/www/prolog_tutorial/2_8.html
        dollarChangeDefinition = "change([H,Q,D,N,P]) :-
                            member(H,[0,1,2]),                      /* Half-dollars */
                            member(Q,[0,1,2,3,4]),                  /* quarters     */
                            member(D,[0,1,2,3,4,5,6,7,8,9,10]) ,    /* dimes        */
                            member(N,[0,1,2,3,4,5,6,7,8,9,10,       /* nickels      */
                                       11,12,13,14,15,16,17,18,19,20]),
                            S is 50*H + 25*Q +10*D + 5*N,
                            S =< 100,
                            P is 100-S. "
        
        pengineOptions =
            server: "http://localhost:3020/pengine",
            #Not sure if it is ideal to have "swish" here, dunno what it really affects
            application: "swish",
            #The json-s is necessary to avoid internal server error when retreiving different types, like literal(...)
            format: "json",
            destroy: false,
            chunk: 1,
            sourceText: dollarChangeDefinition
            #Find the seventh number in the fibonacci sequence
            ask: 'change([H,Q,D,N,P]).'
        
        peng = new pengines(pengineOptions)
            .on('create', -> )
            .on('error',(obj) -> test.done())
            .on('failure',(obj) -> test.done())
            .on('success', (result) ->
                coins = result.data[0]
                noCents = coins.H*50 + coins.Q*25 + coins.D*10 + coins.N*5 + coins.P
                
                test.ok(noCents==100,"Number of cents add up to 100")
                if result.more
                    peng.next()
                else
                    console.log "Done"
                    test.done())
    
    fibonacci : (test) ->
        test.expect(1)
    
        fibonacciDefintion = "fib(1,1).
                        fib(2,2).
                        fib(N,I) :-
                            N>2,
                            N1 is N-1,
                            N2 is N-2,
                            fib(N1,I1),
                            fib(N2,I2),
                            I is I1 + I2."
        

        
        pengineOptions =
            server: "http://localhost:3020/pengine",
            #Not sure if it is ideal to have "swish" here, dunno what it really affects
            application: "swish",
            #The json-s is necessary to avoid internal server error when retreiving different types, like literal(...)
            format: "json",
            destroy: true,
            chunk: 1,
            sourceText: fibonacciDefintion
            #Find the seventh number in the fibonacci sequence
            ask: 'fib(7,N)'


        
        peng = new pengines(pengineOptions)
            .on('create', -> )
            .on('error',(obj) -> test.ok(false,"Failed: #{obj.data}"))
            .on('failure',(obj) -> test.ok(false,"Failed: #{obj.data}"))
            .on('success', (result) ->
                test.ok(result.data[0].N == 21,"Fibonacci number 7 == 21")
                test.done())

module.exports = tests
